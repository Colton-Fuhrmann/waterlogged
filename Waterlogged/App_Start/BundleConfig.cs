﻿using System.Web;
using System.Web.Optimization;

namespace Waterlogged
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryUnobtrusiveAjax").Include(
                        "~/Scripts/jquery.unobtrusive-ajax.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            // Style Bundles
            bundles.Add(new StyleBundle("~/Content/Styles/Bootstrap").Include(
                      "~/Content/Styles/Bootstrap/bootstrap.css",
                      "~/Content/Styles/Bootstrap/bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/Content/Styles/Custom").Include(
                      "~/Content/Styles/Custom/Site.css",
                      "~/Content/Styles/Custom/Waterlogged.css"));
        }
    }
}
