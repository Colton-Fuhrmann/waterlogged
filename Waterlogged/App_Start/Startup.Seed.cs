﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using Waterlogged.Data;
using Waterlogged.Data.Auth;
using Waterlogged.Data.Models;

namespace Waterlogged
{
    public partial class Startup
    {
        private string _testUser = "colton.fuhrmann@gmail.com";

        public void Seed()
        {
            var db = WaterloggedDbContext.Create();

            // if the test admin user has already been added to the db, do not seed
            var testAdminUser = db.Users.FirstOrDefault(x => x.UserName == _testUser);
            if (testAdminUser != null)
            {
                return;
            }
            else
            {
                SeedAdminUser();
                Workouts();
            }
        }

        private void SeedAdminUser()
        {
            var db = WaterloggedDbContext.Create();

            var user = new WaterloggedAppUser
            {
                UserName = _testUser,
                Email = _testUser,
                EmailConfirmed = true,
                LockoutEnabled = false,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            if (!db.Users.Any(u => u.UserName == user.UserName))
            {
                var password = new PasswordHasher();
                var hashed = password.HashPassword("password");
                user.PasswordHash = hashed;
                var userStore = new CustomUserStore(db);
                userStore.CreateAsync(user);
            }
        }

        private void Workouts()
        {
            var db = WaterloggedDbContext.Create();

            var user = db.Users.FirstOrDefault(x => x.UserName == _testUser);
            db.SetUserId(user.Id);
            var yardsDistance = db.DistanceUnits.FirstOrDefault(x => x.Name == "yards");
            var breathRest = db.RestTypes.FirstOrDefault(x => x.Name == "Breaths");
            var freestyleStroke = db.StrokeTypes.FirstOrDefault(x => x.Name == "Freestyle");

            // Week 1
            DateTime startDate = DateTime.Now;
            var workout1 = new Workout { Name = "Week 1", UserId = user.Id, StartDate = startDate };
            workout1.WorkoutSets = new List<WorkoutSet>()
            {
                CreateWorkoutSet(1, 4, 100, yardsDistance.Id, 12, breathRest.Id, freestyleStroke.Id),
                CreateWorkoutSet(2, 4, 50, yardsDistance.Id, 8, breathRest.Id, freestyleStroke.Id),
                CreateWorkoutSet(3, 4, 25, yardsDistance.Id, 4, breathRest.Id, freestyleStroke.Id)
            };

            // Week 2
            startDate = startDate.AddDays(7);
            var workout2 = new Workout { Name = "Week 2", UserId = user.Id, StartDate = startDate };
            workout2.WorkoutSets = new List<WorkoutSet>()
            {
                CreateWorkoutSet(1, 1, 200, yardsDistance.Id, 12, breathRest.Id, freestyleStroke.Id),
                CreateWorkoutSet(2, 4, 100, yardsDistance.Id, 10, breathRest.Id, freestyleStroke.Id),
                CreateWorkoutSet(3, 4, 50, yardsDistance.Id, 6, breathRest.Id, freestyleStroke.Id),
                CreateWorkoutSet(4, 4, 25, yardsDistance.Id, 4, breathRest.Id, freestyleStroke.Id)
            };

            // Week 3
            startDate = startDate.AddDays(7);
            var workout3 = new Workout { Name = "Week 3", UserId = user.Id, StartDate = startDate };
            workout3.WorkoutSets = new List<WorkoutSet>()
            {
                CreateWorkoutSet(1, 1, 400, yardsDistance.Id, 12, breathRest.Id, freestyleStroke.Id),
                CreateWorkoutSet(2, 1, 200, yardsDistance.Id, 10, breathRest.Id, freestyleStroke.Id),
                CreateWorkoutSet(3, 4, 100, yardsDistance.Id, 8, breathRest.Id, freestyleStroke.Id),
                CreateWorkoutSet(4, 4, 50, yardsDistance.Id, 4, breathRest.Id, freestyleStroke.Id)
            };

            // Week 4
            startDate = startDate.AddDays(7);
            var workout4 = new Workout { Name = "Week 4", UserId = user.Id, StartDate = startDate };
            workout4.WorkoutSets = new List<WorkoutSet>()
            {
                CreateWorkoutSet(1, 1, 600, yardsDistance.Id, 10, breathRest.Id, freestyleStroke.Id),
                CreateWorkoutSet(2, 1, 300, yardsDistance.Id, 8, breathRest.Id, freestyleStroke.Id),
                CreateWorkoutSet(3, 4, 100, yardsDistance.Id, 6, breathRest.Id, freestyleStroke.Id),
                CreateWorkoutSet(4, 4, 50, yardsDistance.Id, 4, breathRest.Id, freestyleStroke.Id)
            };

            // Week 5
            startDate = startDate.AddDays(7);
            var workout5 = new Workout { Name = "Week 5", UserId = user.Id, StartDate = startDate };
            workout5.WorkoutSets = new List<WorkoutSet>()
            {
                CreateWorkoutSet(1, 1, 1000, yardsDistance.Id, 8, breathRest.Id, freestyleStroke.Id),
                CreateWorkoutSet(2, 4, 100, yardsDistance.Id, 4, breathRest.Id, freestyleStroke.Id),
                CreateWorkoutSet(3, 4, 50, yardsDistance.Id, 4, breathRest.Id, freestyleStroke.Id)
            };

            // Week 6
            startDate = startDate.AddDays(7);
            var workout6 = new Workout { Name = "Week 6", UserId = user.Id, StartDate = startDate };
            workout6.WorkoutSets = new List<WorkoutSet>()
            {
                CreateWorkoutSet(1, 1, 1200, yardsDistance.Id, 6, breathRest.Id, freestyleStroke.Id),
                CreateWorkoutSet(2, 3, 100, yardsDistance.Id, 4, breathRest.Id, freestyleStroke.Id),
                CreateWorkoutSet(3, 3, 50, yardsDistance.Id, 4, breathRest.Id, freestyleStroke.Id)
            };

            db.Workouts.AddRange(new List<Workout> { workout1, workout2, workout3, workout4, workout5, workout6 });
            db.SaveChanges();
        }    
        
        private WorkoutSet CreateWorkoutSet(int orderInWorkout, int repQuantity, int repDistance, int distanceUnitId,
                                int restQuantity, int restTypeId, int strokeTypeId)
        {
            var rest = new Rest { Quantity = restQuantity, RestTypeId = restTypeId };
            var workoutSet = new WorkoutSet
            {
                Distance = repDistance,
                DistanceUnitId = distanceUnitId,
                OrderInWorkout = orderInWorkout,
                Repetitions = repQuantity,
                Rest = rest,
                StrokeTypeId = strokeTypeId              
            };
            
            return workoutSet;
        }     
    }
}