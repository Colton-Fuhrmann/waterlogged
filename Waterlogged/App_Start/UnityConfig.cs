﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;
using Waterlogged.Controllers;
using Waterlogged.Data;
using Waterlogged.Data.Auth;

namespace Waterlogged
{
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            return new UnityContainer();
        });

        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        public static void RegisterTypes()
        {
            var container = GetConfiguredContainer();
            container.RegisterTypes(
                AllClasses.FromAssemblies(new[]
                {
                    Assembly.Load("Waterlogged.Data"),
                    Assembly.Load("Waterlogged")
                }),
                WithMappings.FromMatchingInterface,
                WithName.Default);

            // TODO: look into this way of setting up Unity with OwinContext http://tech.trailmax.info/2014/09/aspnet-identity-and-ioc-container-registration/
            // Container registering code from: https://stackoverflow.com/a/24806338
            container.RegisterType<DbContext, WaterloggedDbContext>(new PerRequestLifetimeManager());
            container.RegisterType<UserManager<WaterloggedAppUser, int>>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserStore<WaterloggedAppUser, int>, CustomUserStore>(new HierarchicalLifetimeManager());
            container.RegisterType<AccountController>(new InjectionConstructor());
            container.RegisterType<ManageController>(new InjectionConstructor());
        }

        public static void RegisterAutoMapper(IMapper mapper)
        {
            var container = GetConfiguredContainer();
            container.RegisterInstance(mapper);
        }
    }
}