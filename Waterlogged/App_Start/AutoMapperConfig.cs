﻿using AutoMapper;
using Waterlogged.Models;
using Waterlogged.Data.Models;

namespace Waterlogged
{
    public static class AutoMapperConfig
    {
        public static IMapper Initialize()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.SourceMemberNamingConvention = new PascalCaseNamingConvention();
                cfg.DestinationMemberNamingConvention = new PascalCaseNamingConvention();

                // configure Workout
                cfg.CreateMap<Workout, DeleteWorkout>();
                cfg.CreateMap<Workout, DetailsWorkout>();
                cfg.CreateMap<Workout, EditWorkout>();
                cfg.CreateMap<Workout, IndexWorkout>();
                cfg.CreateMap<Workout, WorkoutEntry>();

                // configure WorkoutSet
                cfg.CreateMap<WorkoutSet, DetailsSet>();
                cfg.CreateMap<WorkoutSet, EditSet>();
            });

            return config.CreateMapper();
        }
    }
}