﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;
using Waterlogged.ActionResults;

namespace Waterlogged.Controllers
{
    public class BaseController : Controller
    {
        public IMapper Mapper { get; private set; }

        public int UserId
        {
            get
            {
                return System.Web.HttpContext.Current.User.Identity.GetUserId<int>();
            }
        }

        public BaseController(IMapper mapper)
        {
            Mapper = mapper;
        }

        protected AutoMapViewResult AutoMapView<TDestination>(object domainModel, ViewResultBase viewResult)
        {
            return new AutoMapViewResult(domainModel.GetType(), typeof(TDestination), domainModel, viewResult, Mapper);
        }
    }
}