﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Waterlogged.Data;

namespace Waterlogged.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var context = WaterloggedDbContext.Create();
            var restTypes = context.RestTypes.FirstOrDefault();

            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}