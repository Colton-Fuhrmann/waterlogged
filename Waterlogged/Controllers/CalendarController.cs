﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Waterlogged.BLL.Interfaces;
using Waterlogged.Data;
using Waterlogged.Data.Models;
using Waterlogged.Models;
using Waterlogged.ViewModelHelpers;

namespace Waterlogged.Controllers
{
    public class CalendarController : BaseController
    {
        private WaterloggedDbContext _db;
        private IWorkoutLogic _workoutLogic;
        private IMapper _mapper;
        private WorkoutSetHelper _workoutSetHelper;

        public CalendarController(WaterloggedDbContext db, IWorkoutLogic workoutLogic, IMapper mapper, WorkoutSetHelper workoutSetHelper) : base(mapper)
        {
            _db = db;
            _db.SetUserId(UserId);
            _workoutLogic = workoutLogic;
            _workoutSetHelper = workoutSetHelper;
            _mapper = mapper;
        }

        // GET: Calendar
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetWorkouts()
        {
            var workouts = _mapper.Map<List<Workout>, List<EditWorkout>>(_db.Workouts.ToList());
            return new JsonResult { Data = workouts, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public JsonResult SaveWorkout(EditWorkout workout)
        {
            bool status = false;
            if (!ModelState.IsValid)
            {
                return new JsonResult { Data = new { status = status } };
            }

            int workoutId = workout.Id;
            if (workout.Id > 0)
            {
                var workoutToModify = _db.Workouts.Find(workout.Id);
                _workoutLogic.EditWorkout(workout, workoutToModify);
                status = true;
            }
            else
            {
                workoutId = _workoutLogic.CreateWorkout(workout);
                status = true;
            }
            return new JsonResult { Data = new { status = status, workoutId = workoutId } };
        }

        [HttpPost]
        public JsonResult DeleteWorkout(int workoutId)
        {
            var status = false;
            var workoutToRemove = _db.Workouts.FirstOrDefault(x => x.Id == workoutId);
            if (workoutToRemove != null)
            {
                _db.Workouts.Remove(workoutToRemove);
                _db.SaveChanges();
                status = true;
            }
            return new JsonResult { Data = new { status = status } };
        }

        public ActionResult EditWorkout(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Workout workout = _db.Workouts.Find(id);
            if (workout == null)
            {
                return new HttpNotFoundResult();
            }

            var editWorkout = _mapper.Map<EditWorkout>(workout);
            foreach (var set in editWorkout.WorkoutSets)
            {
                set.DistanceUnits = _workoutSetHelper.DistanceUnitsSelectList;
                set.RestTypes = _workoutSetHelper.RestTypesSelectList;
                set.StrokeTypes = _workoutSetHelper.StrokeTypesSelectList;
            }

            return View("CalEditWorkout", editWorkout);
        }

        public ActionResult AddWorkout(DateTime? start, DateTime? end)
        {
            if (!start.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var editWorkout = new EditWorkout { StartDate = start.Value };
            foreach (var set in editWorkout.WorkoutSets)
            {
                set.DistanceUnits = _workoutSetHelper.DistanceUnitsSelectList;
                set.RestTypes = _workoutSetHelper.RestTypesSelectList;
                set.StrokeTypes = _workoutSetHelper.StrokeTypesSelectList;
            }

            return View("CalEditWorkout", editWorkout);
        }

        public ActionResult WorkoutDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Workout workout = _db.Workouts.Find(id);
            if (workout == null)
            {
                return new HttpNotFoundResult();
            }

            var editWorkout = _mapper.Map<DetailsWorkout>(workout);
            return View("CalWorkoutDetails", editWorkout);
        }
    }
}