﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Waterlogged.BLL.Interfaces;
using Waterlogged.Data;
using Waterlogged.Data.Models;
using Waterlogged.HtmlHelpers;
using Waterlogged.Models;
using Waterlogged.ViewModelHelpers;

namespace Waterlogged.Controllers
{
    public class WorkoutsController : BaseController
    {
        private WaterloggedDbContext _db;
        private IWorkoutLogic _workoutLogic;
        private IMapper _mapper;
        private WorkoutSetHelper _workoutSetHelper;

        public WorkoutsController(WaterloggedDbContext db, IWorkoutLogic workoutLogic, IMapper mapper, WorkoutSetHelper workoutSetHelper) : base(mapper)
        {
            _db = db;
            _db.SetUserId(UserId);

            _workoutLogic = workoutLogic;
            _mapper = mapper;
            _workoutSetHelper = workoutSetHelper;
        }

        // GET: Workouts
        public ActionResult Index()
        {
            var indexWorkouts = _mapper.Map<List<Workout>, List<IndexWorkout>>(_db.Workouts.ToList());
            return View(indexWorkouts);
        }

        // GET: Workouts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Workout workout = _db.Workouts.Find(id);
            if (workout == null)
            {
                return HttpNotFound();
            }

            return AutoMapView<DetailsWorkout>(workout, View());
        }

        // GET: Workouts/Create
        public ActionResult Create()
        {
            var newWorkout = new EditWorkout();
            var set = new EditSet();
            set.DistanceUnits = _workoutSetHelper.DistanceUnitsSelectList;
            set.RestTypes = _workoutSetHelper.RestTypesSelectList;
            set.StrokeTypes = _workoutSetHelper.StrokeTypesSelectList;
            newWorkout.WorkoutSets.Add(set);

            return View(newWorkout);
        }

        // POST: Workouts/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EditWorkout createWorkout)
        {
            if (ModelState.IsValid)
            {
                _workoutLogic.CreateWorkout(createWorkout);
                return RedirectToAction("Index");
            }

            return View(createWorkout);
        }

        // GET: Workouts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Workout workout = _db.Workouts.Find(id);
            if (workout == null)
            {
                return new HttpNotFoundResult();
            }

            var editWorkout = _mapper.Map<EditWorkout>(workout);
            foreach (var set in editWorkout.WorkoutSets)
            {
                set.DistanceUnits = _workoutSetHelper.DistanceUnitsSelectList;
                set.RestTypes = _workoutSetHelper.RestTypesSelectList;
                set.StrokeTypes = _workoutSetHelper.StrokeTypesSelectList;
            }

            return View(editWorkout);
        }

        // POST: Workouts/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditWorkout editWorkout)
        {
            var workout = _db.Workouts.Find(editWorkout.Id);
            if (workout == null)
            {
                return new HttpNotFoundResult();
            }

            if (ModelState.IsValid)
            {
                _workoutLogic.EditWorkout(editWorkout, workout);
                return RedirectToAction("Index");
            }
            return View(editWorkout);
        }

        // GET: Workouts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            Workout workout = _db.Workouts.Find(id);
            if (workout == null)
            {
                return HttpNotFound();
            }

            return AutoMapView<DeleteWorkout>(workout, View());
        }

        // POST: Workouts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            bool success = _workoutLogic.DeleteWorkout(id);
            if (success) { return RedirectToAction("Index"); }

            return RedirectToAction("Index", new { Message = "Delete failed" });
        }

        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "")]
        public ActionResult AddSet()
        {
            var set = new EditSet();
            set.DistanceUnits = _workoutSetHelper.DistanceUnitsSelectList;
            set.RestTypes = _workoutSetHelper.RestTypesSelectList;
            set.StrokeTypes = _workoutSetHelper.StrokeTypesSelectList;

            var setContainer = new EditWorkout();
            setContainer.WorkoutSets.Add(set);
            return View(setContainer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
