﻿var savedWorkoutId = 0;
function WorkoutSaved(data) {
    if (data.status) {
        savedWorkoutId = data.workoutId;
        $('#workoutSavedBtn').click();
    }
    else {
        alert('Error saving');
    }
}

$(document).ready(function () {
    const dateTimeFormat = 'MM/DD/YYYY HH:mm A';
    var workouts = [];
    var selectedWorkout = null;
    FetchWorkoutsAndRenderCalendar();

    function FetchWorkoutsAndRenderCalendar() {
        workouts = [];
        $.ajax({
            type: "GET",
            url: "/calendar/GetWorkouts",
            success: function (data) {
                $.each(data, function (i, v) {
                    workouts.push({
                        id: v.Id,
                        title: v.Name,
                        name: v.Name,
                        start: moment(v.StartDate),
                        end: v.StartDate != null ? moment(v.StartDate) : null,
                        workoutSets: v.WorkoutSets,
                        color: '#75C5DB',
                        allDay: false
                    });
                })

                GenerateCalendar(workouts);
            },
            error: function (error) {
                alert('failed: ' + error);
            }
        })
    }

    function GenerateCalendar(workouts) {
        // if there is already an instance of the calendar, destroy it
        $('#calendar').fullCalendar('destroy');

        $('#calendar').fullCalendar({
            contentHeight: 400,
            defaultDate: new Date(),
            timeFormat: 'h(:mm)a',
            header: {
                left: 'prev, next, today',
                center: 'title',
                right: 'month, basicWeek, basicDay, agenda'
            },
            eventLimit: true,
            eventColor: '#75C5DB',
            events: workouts,
            eventClick: function (calEvent, jsEvent, view) {
                selectedWorkout = calEvent;
                $('#workoutModal #workoutName').text(calEvent.name);
                PopulateWorkoutDetails();
                DisplayWorkoutModal(true);
            },
            selectable: true,
            select: function (start, end) {
                selectedWorkout = {
                    start: start.format(dateTimeFormat),
                    end: end.format(dateTimeFormat)
                };
                OpenAddForm();
                $('#calendar').fullCalendar('unselect');
            },
            editable: true,
            eventDrop: function (calEvent) {
                var data = {
                    Id: calEvent.id,
                    Name: calEvent.name,
                    StartDate: calEvent.start.format(dateTimeFormat),
                    WorkoutSets: calEvent.workoutSets
                };
                SaveWorkout(data);
            }
        })
    }

    $('#btnEdit').click(function () {
        // Open edit modal
        OpenEditForm();
    });

    $('#btnDelete').click(function () {
        if (selectedWorkout != null && confirm("Are you sure?")) {
            $.ajax({
                type: "POST",
                url: "/Calendar/DeleteWorkout",
                data: { 'workoutId': selectedWorkout.id },
                success: function (data) {
                    if (data.status) {
                        // Refresh the calendar
                        FetchWorkoutsAndRenderCalendar();
                        DisplayWorkoutModal(false);
                    }
                },
                error: function () {
                    alert('Delete failed');
                }
            });
        }
    });

    function OpenEditForm() {
        if (selectedWorkout != null) {
            $.ajax({
                type: "GET",
                url: "/Calendar/EditWorkout",
                data: { 'id': selectedWorkout.id },
                success: function (data) {
                    PopulateEditWorkoutContainer(data);
                },
                error: function () {
                    alert('Open Edit form failed');
                }
            });
        }
        DisplayWorkoutModal(false);
        DisplaySaveWorkoutModal(true);
    }

    function OpenAddForm() {
        if (selectedWorkout != null) {
            $.ajax({
                type: "GET",
                url: "/Calendar/AddWorkout",
                data: {
                    'start': selectedWorkout.start,
                    'end': selectedWorkout.end
                },
                success: function (data) {
                    PopulateEditWorkoutContainer(data);
                    $('#saveWorkoutModal #editWorkoutContainer #workoutStartDate');
                },
                error: function () {
                    alert('Open Add form failed');
                }
            });
        }
        DisplayWorkoutModal(false);
        DisplaySaveWorkoutModal(true);
    }

    function PopulateEditWorkoutContainer(editPartialView) {
        $('#saveWorkoutModal #editWorkoutContainer').empty().html(editPartialView);
        // apply moment.js datetimepicker script to the workoutStartDate input
        $('#saveWorkoutModal #editWorkoutContainer #workoutStartDate').datetimepicker({
            format: dateTimeFormat
        });
    }

    function PopulateWorkoutDetails() {
        if (selectedWorkout != null) {
            $.ajax({
                type: "GET",
                url: "/Calendar/WorkoutDetails",
                data: {
                    'id': selectedWorkout.id
                },
                success: function (data) {
                    $('#workoutModal #workoutDetails').empty().html(data);
                },
                error: function () {
                    alert('Get Workout details failed');
                }
            });
        }
    }

    function DisplayWorkoutModal(shouldDisplay) {
        shouldDisplay ? $('#workoutModal').modal() : $('#workoutModal').modal('hide');
    }

    function DisplaySaveWorkoutModal(shouldDisplay) {
        shouldDisplay ? $('#saveWorkoutModal').modal() : $('#saveWorkoutModal').modal('hide');
    }

    // delegate the click event on #add-set since it doesn't exist when the Calendar Index.cshtml is first loaded 
    $('#saveWorkoutModal').on('click', '#add-set', function () {
        jQuery.get('/Workouts/AddSet').done(function (html) {
            $('#set-list').append(html);
        });
    });

    // this code is manually triggered with a click after the form in CalEditWorkout.cshtml calls WorkoutSaved or when SaveWorkout is successful
    $('#saveWorkoutModal').on('click', '#workoutSavedBtn', function () {
        // update the calendar
        FetchWorkoutsAndRenderCalendar();

        // update the workoutModal container with the newly created/updated workout
        selectedWorkout.id = savedWorkoutId;
        PopulateWorkoutDetails();

        DisplaySaveWorkoutModal(false);
        DisplayWorkoutModal(true);
    });

    function SaveWorkout(data) {
        $.ajax({
            type: 'POST',
            url: '/Calendar/SaveWorkout',
            data: data,
            success: function (data) {
                if (data.status) {
                    FetchWorkoutsAndRenderCalendar();
                }
                else {
                    alert('Error saving');
                }
            },
            error: function () {
                alert('Save failed');
            }
        });
    }
})