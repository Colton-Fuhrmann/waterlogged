﻿using Microsoft.Owin;
using Owin;
using System.Threading.Tasks;
using Waterlogged.Data;

[assembly: OwinStartupAttribute(typeof(Waterlogged.Startup))]
namespace Waterlogged
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Startup.Auth
            ConfigureAuth(app);

            // Startup.Seed
            Seed();
        }
    }
}
