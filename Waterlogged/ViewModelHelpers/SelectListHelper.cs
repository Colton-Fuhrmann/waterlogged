﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Waterlogged.ViewModelHelpers
{
    public static class SelectListHelper
    {
        public static IEnumerable<SelectListItem> CreateSelectList<T>(IEnumerable<T> entities, Func<T, object> funcToGetValue, Func<T, object> funcToGetText)
        {
            return entities
                    .Select(x => new SelectListItem
                    {
                        Value = funcToGetValue(x).ToString(),
                        Text = funcToGetText(x).ToString()
                    }).ToList();
        }

        public static IEnumerable<SelectListItem> CreateSelectList<T>(IEnumerable<T> entities, Func<T, object> funcToGetValue, Func<T, object> funcToGetText, object selectedValue)
        {
            return entities
                    .Select(x => new SelectListItem
                    {
                        Value = funcToGetValue(x).ToString(),
                        Text = funcToGetText(x).ToString(),
                        Selected = selectedValue != null ? ((funcToGetValue(x).ToString() == selectedValue.ToString()) ? true : false) : false
                    }).ToList();
        }

        public static IEnumerable<SelectListItem> CreateSelectList<T>(IEnumerable<T> entities, Func<T, bool> whereClause, Func<T, object> funcToGetValue, Func<T, object> funcToGetText, object selectedValue)
        {
            return entities
                    .Where(whereClause)
                    .Select(x => new SelectListItem
                    {
                        Value = funcToGetValue(x).ToString(),
                        Text = funcToGetText(x).ToString(),
                        Selected = selectedValue != null ? ((funcToGetValue(x).ToString() == selectedValue.ToString()) ? true : false) : false
                    }).ToList();
        }
    }
}