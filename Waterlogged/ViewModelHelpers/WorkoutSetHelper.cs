﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Waterlogged.Data;
using Waterlogged.Data.Models;

namespace Waterlogged.ViewModelHelpers
{
    public class WorkoutSetHelper
    {
        private WaterloggedDbContext _db;

        private static IEnumerable<SelectListItem> _distanceUnitsSelectList;
        private static IEnumerable<SelectListItem> _restTypesSelectList;
        private static IEnumerable<SelectListItem> _strokeTypesSelectList;

        public WorkoutSetHelper(WaterloggedDbContext db)
        {
            _db = db;
        }

        public IEnumerable<SelectListItem> DistanceUnitsSelectList
        {
            get
            {
                if (_distanceUnitsSelectList != null) { return _distanceUnitsSelectList; }

                var distanceUnits = SelectListHelper.CreateSelectList<DistanceUnit>(_db.DistanceUnits, x => x.Id, x => x.Name);
                _distanceUnitsSelectList = new SelectList(distanceUnits, "Value", "Text");
                return _distanceUnitsSelectList;
            }

            set { _distanceUnitsSelectList = value; }
        }

        public IEnumerable<SelectListItem> RestTypesSelectList
        {
            get
            {
                if (_restTypesSelectList != null) { return _restTypesSelectList; }

                var restTypes = SelectListHelper.CreateSelectList<RestType>(_db.RestTypes, x => x.Id, x => x.Name);
                return new SelectList(restTypes, "Value", "Text");
            }

            set { _restTypesSelectList = value; }
        }

        public IEnumerable<SelectListItem> StrokeTypesSelectList
        {
            get
            {
                if (_strokeTypesSelectList != null) { return _strokeTypesSelectList; }

                var strokeTypes = SelectListHelper.CreateSelectList<StrokeType>(_db.StrokeTypes, x => x.Id, x => x.Name);
                return new SelectList(strokeTypes, "Value", "Text");
            }

            set { _strokeTypesSelectList = value; }
        }
    }
}