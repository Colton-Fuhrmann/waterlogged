﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using System.Web;
using Waterlogged.BLL.Interfaces;
using Waterlogged.Data;
using Waterlogged.Data.Models;
using Waterlogged.Models;
using System.Data.Entity;

namespace Waterlogged.BLL.Implementations
{
    public class WorkoutLogic : IWorkoutLogic
    {
        private WaterloggedDbContext _db;

        public WorkoutLogic(WaterloggedDbContext db)
        {
            _db = db;
        }

        public int CreateWorkout(EditWorkout workout)
        {
            Workout newWorkout = new Workout
            {
                Name = workout.Name,
                StartDate = workout.StartDate,
                //TODO: maybe isolate httpcontext call into a helper class
                UserId = HttpContext.Current.User.Identity.GetUserId<int>()
            };

            var restQuantities = workout.WorkoutSets.Select(x => x.RestQuantity);
            var existingRests = from rests in _db.Rests where restQuantities.Contains(rests.Quantity) select rests;

            foreach (var set in workout.WorkoutSets)
            {
                // attempt to use an existing Rest as to not create a duplicate
                var restToAddToWorkoutSet = existingRests.FirstOrDefault(x => x.Quantity == set.RestQuantity && x.RestTypeId == set.RestRestTypeId);
                if (restToAddToWorkoutSet == null)
                {
                    restToAddToWorkoutSet = new Rest
                    {
                        Quantity = set.RestQuantity,
                        RestTypeId = set.RestRestTypeId
                    };
                }

                var newWorkoutSet = new WorkoutSet
                {
                    Distance = set.Distance,
                    DistanceUnitId = set.DistanceUnitId,
                    OrderInWorkout = set.OrderInWorkout,
                    Repetitions = set.Repetitions,
                    Rest = restToAddToWorkoutSet,
                    StrokeTypeId = set.StrokeTypeId,
                };

                newWorkout.WorkoutSets.Add(newWorkoutSet);
            }

            var newWorkoutFromDb = _db.Workouts.Add(newWorkout);
            _db.SaveChanges();

            return newWorkoutFromDb.Id;
        }

        public void EditWorkout(EditWorkout editWorkout, Workout workoutToModify)
        {
            // TODO: Research patterns on null validation and best practice to handle the exceptions -- if I keep this here, it would be dumb to return null -- I would need to handle the exception somehow
            //       Also look more into the row-level security regarding the UserId and validating that they aren't editing what doesn't belong to them
            if (editWorkout == null || workoutToModify == null || workoutToModify.UserId != HttpContext.Current.User.Identity.GetUserId<int>())
            {
                return;
            }

            // copy over Workout properties from edit model
            workoutToModify.Name = editWorkout.Name;
            workoutToModify.StartDate = editWorkout.StartDate;

            var restQuantities = editWorkout.WorkoutSets.Select(x => x.RestQuantity);
            var existingRests = from rests in _db.Rests where restQuantities.Contains(rests.Quantity) select rests;

            foreach (var set in editWorkout.WorkoutSets)
            {
                WorkoutSet setToModify = null;
                if (set.Id <= 0)
                {
                    setToModify = new WorkoutSet();
                    _db.WorkoutSets.Attach(setToModify);
                    _db.Entry(setToModify).State = EntityState.Added;
                }
                else
                {
                    setToModify = workoutToModify.WorkoutSets.FirstOrDefault(x => x.Id == set.Id);
                }

                if (setToModify != null)
                {
                    // attempt to use an existing Rest as to not create a duplicate
                    var restToAddToWorkoutSet = existingRests.FirstOrDefault(x => x.Quantity == set.RestQuantity && x.RestTypeId == set.RestRestTypeId);
                    if (restToAddToWorkoutSet == null)
                    {
                        restToAddToWorkoutSet = new Rest
                        {
                            Quantity = set.RestQuantity,
                            RestTypeId = set.RestRestTypeId
                        };
                    }
                    setToModify.Rest = restToAddToWorkoutSet;

                    setToModify.Distance = set.Distance;
                    setToModify.DistanceUnitId = set.DistanceUnitId;
                    setToModify.OrderInWorkout = set.OrderInWorkout;
                    setToModify.Repetitions = set.Repetitions;
                    setToModify.StrokeTypeId = set.StrokeTypeId;

                    if (set.Id <= 0)
                    {
                        workoutToModify.WorkoutSets.Add(setToModify);
                    }
                }
            }

            _db.Entry(workoutToModify).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public bool DeleteWorkout(int workoutId)
        {
            bool status = false;
            var workoutToRemove = _db.Workouts.FirstOrDefault(x => x.Id == workoutId);
            if (workoutToRemove != null)
            {
                _db.Workouts.Remove(workoutToRemove);
                _db.SaveChanges();
                status = true;
            }

            return status;
        }
    }
}