﻿using Waterlogged.Data.Models;
using Waterlogged.Models;

namespace Waterlogged.BLL.Interfaces
{
    public interface IWorkoutLogic
    {
        int CreateWorkout(EditWorkout workout);
        void EditWorkout(EditWorkout editWorkout, Workout workoutToModify);
        bool DeleteWorkout(int workoutId);
    }
}
