﻿using System;
using System.ComponentModel;

namespace Waterlogged.Models
{
    public class IndexWorkout
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [DisplayName("Date Created")]
        public DateTime? DateCreated { get; set; }
    }
}