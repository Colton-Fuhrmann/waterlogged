﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace Waterlogged.Models
{
    public class DetailsWorkout
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        public string Name { get; set; }

        [DisplayName("Workout Date")]
        public DateTime StartDate { get; set; }

        [DisplayName("Date Created")]
        public DateTime? DateCreated { get; set; }

        public IEnumerable<DetailsSet> WorkoutSets { get; set; }
    }
}