﻿using System.Web.Mvc;

namespace Waterlogged.Models
{
    public class DeleteWorkout
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}