﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Waterlogged.Models
{
    public class EditWorkout
    {
        public EditWorkout()
        {
            this.WorkoutSets = new List<EditSet>();
        }

        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        public IList<EditSet> WorkoutSets { get; set; }
    }
}