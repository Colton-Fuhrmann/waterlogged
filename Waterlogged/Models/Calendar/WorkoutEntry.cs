﻿using System;
using System.Collections.Generic;

namespace Waterlogged.Models
{
    public class WorkoutEntry
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? DateCreated { get; set; }
        public IList<DetailsSet> WorkoutSets { get; set; }
    }
}