﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace Waterlogged.Models
{
    public class EditSet
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        // WorkoutSet Entity portion
        public int Repetitions { get; set; }
        public int OrderInWorkout { get; set; }

        // Repetition Entity portion
        public double Distance { get; set; }
        public int DistanceUnitId { get; set; }
        public IEnumerable<SelectListItem> DistanceUnits { get; set; }

        [DisplayName("Rest")]
        public int RestQuantity { get; set; }

        public int RestRestTypeId { get; set; }
        public IEnumerable<SelectListItem> RestTypes { get; set; }

        [DisplayName("Stroke")]
        public int StrokeTypeId { get; set; }
        public IEnumerable<SelectListItem> StrokeTypes { get; set; }

        // Index is used in EditorForManyHelper.cs
        public string Index { get; set; }
    }
}