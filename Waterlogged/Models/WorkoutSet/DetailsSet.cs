﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Waterlogged.Models
{
    public class DetailsSet
    {  
        // display name for view
        [DisplayName("Set")]
        public string SetLabel { get; set; }

        public int Repetitions { get; set; }

        [DisplayName("Order")]
        public int OrderInWorkout { get; set; }

        public double Distance { get; set; }

        public string DistanceUnitName { get; set; }

        [DisplayName("Rest")]
        public string RestLabel { get; set; }
       
        public int RestQuantity { get; set; }

        public string RestRestTypeName { get; set; }
        
        [DisplayName("Stroke")]
        public string StrokeTypeName{ get; set; }
    }
}