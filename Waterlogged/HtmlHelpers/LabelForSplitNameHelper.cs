﻿using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Waterlogged.HtmlHelpers
{
    public static class LabelForSplitNameHelper
    {
        public static MvcHtmlString LabelForSplitName<T, TResult>(this HtmlHelper<T> helper, Expression<Func<T, TResult>> expression)
        {
            // taken from: http://stackoverflow.com/a/2724443
            string propertyName = PropertyName(expression);
            string labelValue = SplitCamelCase(propertyName);

            string label = String.Format("<label for=\"{0}\">{1}</label>", propertyName, labelValue);
            return MvcHtmlString.Create(label);
        }

        internal static string PropertyName<T, TResult>(Expression<Func<T, TResult>> expression)
        {
            switch (expression.Body.NodeType)
            {
                case ExpressionType.MemberAccess:
                    var memberExpression = expression.Body as MemberExpression;
                    return memberExpression.Member.Name;
                default:
                    return "oops";
            }
        }

        internal static string SplitCamelCase(string camelCaseString)
        {
            string output = System.Text.RegularExpressions.Regex.Replace(
                camelCaseString,
                "([A-Z])",
                " $1",
                RegexOptions.Compiled).Trim();
            return output;
        }
    }
}