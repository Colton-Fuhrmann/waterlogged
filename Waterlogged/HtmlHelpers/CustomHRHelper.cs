﻿using System;
using System.Web.Mvc;

namespace Waterlogged.HtmlHelpers
{
    public static class CustomHRHelper
    {
        private static string _hrClass = "wlog-hr";
        public static MvcHtmlString CustomHR(this HtmlHelper helper)
        {
            string hr = String.Format("<hr class=\"{0}\"/>", _hrClass);
            return MvcHtmlString.Create(hr);
        }
    }
}