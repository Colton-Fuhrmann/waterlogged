﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Waterlogged.Data.Auth;
using Waterlogged.Data.RowLevelSecurity;

namespace Waterlogged.Data.Models
{
    public class Workout : ISecuredByTenant
    {
        public Workout()
        {
            WorkoutSets = new List<WorkoutSet>();
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? DateCreated { get; set; }

        [ForeignKey("UserId")]
        public virtual WaterloggedAppUser User { get; set; }
        public virtual ICollection<WorkoutSet> WorkoutSets { get; set; }
    }
}
