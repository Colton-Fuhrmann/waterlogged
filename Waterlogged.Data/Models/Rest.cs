﻿namespace Waterlogged.Data.Models
{
    public class Rest
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public int RestTypeId { get; set; }

        public virtual RestType RestType { get; set; }
    }
}
