﻿using System.Collections.Generic;
using Waterlogged.Data.RowLevelSecurity;

namespace Waterlogged.Data.Models
{
    public class WorkoutSet : ISecuredByTenant
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int Repetitions { get; set; }
        public int OrderInWorkout { get; set; }
        public double Distance { get; set; }
        public int DistanceUnitId { get; set; }
        public int RestId { get; set; }
        public int StrokeTypeId { get; set; }
        public int WorkoutId { get; set; }

        public virtual DistanceUnit DistanceUnit { get; set; }
        public virtual Rest Rest { get; set; }
        public virtual StrokeType StrokeType { get; set; }
        public virtual Workout Workout { get; set; }
    }
}