﻿namespace Waterlogged.Data.Models
{
    public class DistanceUnit
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
    }
}
