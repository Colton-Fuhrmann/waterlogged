﻿namespace Waterlogged.Data.Models
{
    public class RestType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
