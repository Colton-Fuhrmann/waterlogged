﻿namespace Waterlogged.Data.RowLevelSecurity
{
    public interface ISecuredByTenant
    {
        int UserId { get; set; }
    }
}
