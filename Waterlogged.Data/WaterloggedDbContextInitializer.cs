﻿using System.Collections.Generic;
using Waterlogged.Data.Models;

namespace Waterlogged.Data
{
    public class WaterloggedDbContextInitializer : System.Data.Entity.CreateDatabaseIfNotExists<WaterloggedDbContext>
    {
        protected override void Seed(WaterloggedDbContext context)
        {
            RestTypes(context);
            DistanceUnits(context);
            StrokeTypes(context);
        }

        private void RestTypes(WaterloggedDbContext context)
        {
            var restTypes = new List<RestType>
            {
                new RestType { Name = "None" },
                new RestType { Name = "Breaths" },
                new RestType { Name = "Seconds" },
                new RestType { Name = "Minutes" }
            };
            context.RestTypes.AddRange(restTypes);
            context.SaveChanges();
        }

        private void DistanceUnits(WaterloggedDbContext context)
        {
            var distanceUnits = new List<DistanceUnit>
            {
                new DistanceUnit { Name = "yards", Abbreviation = "yd" },
                new DistanceUnit { Name = "meters", Abbreviation = "m" },
                new DistanceUnit { Name = "kilometer", Abbreviation = "km" },
                new DistanceUnit { Name = "mile", Abbreviation = "mi" }
            };
            context.DistanceUnits.AddRange(distanceUnits);
            context.SaveChanges();
        }

        private void StrokeTypes(WaterloggedDbContext context)
        {
            var strokeTypes = new List<StrokeType>
            {
                new StrokeType { Name = "Freestyle" },
                new StrokeType { Name = "Breast" },
                new StrokeType { Name = "Butterfly" },
                new StrokeType { Name = "Back" },
                new StrokeType { Name = "Side" }
            };
            context.StrokeTypes.AddRange(strokeTypes);
            context.SaveChanges();
        }
    }
}
