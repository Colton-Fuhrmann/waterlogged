﻿using EntityFramework.DynamicFilters;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Threading.Tasks;
using Waterlogged.Data.Auth;
using Waterlogged.Data.Models;
using Waterlogged.Data.RowLevelSecurity;

namespace Waterlogged.Data
{
    public class WaterloggedDbContext : IdentityDbContext<WaterloggedAppUser, CustomRole,
        int, CustomUserLogin, CustomUserRole, CustomUserClaim>
    {
        public const string DateCreatedColumnName = "DateCreated";

        private int _userId = 0;

        public int UserId
        {
            get { return _userId; }
        }

        public void SetUserId(int userId)
        {
            _userId = userId;
            this.SetFilterScopedParameterValue(FilterNames.SECURED_BY_TENANT, _userId);
            this.SetFilterGlobalParameterValue(FilterNames.SECURED_BY_TENANT, _userId);

            var test = this.GetFilterParameterValue(FilterNames.SECURED_BY_TENANT, "UserId");
        }

        public WaterloggedDbContext() : base("WaterloggedContext")
        {
            Database.SetInitializer(new WaterloggedDbContextInitializer());
        }

        public DbSet<DistanceUnit> DistanceUnits { get; set; }
        public DbSet<Rest> Rests { get; set; }
        public DbSet<RestType> RestTypes { get; set; }
        public DbSet<StrokeType> StrokeTypes { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<WorkoutSet> WorkoutSets { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Filter(FilterNames.SECURED_BY_TENANT,
                                (ISecuredByTenant securedByTenant, int userId) => (securedByTenant.UserId == userId),
                                () => 0);
        }

        public override int SaveChanges()
        {
            AddDateCreated();
            HandleNewISecuredByTenantEntries();

            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync()
        {
            AddDateCreated();
            HandleNewISecuredByTenantEntries();

            return base.SaveChangesAsync();
        }

        private void AddDateCreated()
        {
            DateTime saveTime = DateTime.UtcNow;
            foreach (var entry in this.ChangeTracker.Entries().Where(e => e.State == EntityState.Added))
            {
                if (entry.CurrentValues.PropertyNames.Contains(DateCreatedColumnName) && entry.Property(DateCreatedColumnName).CurrentValue == null)
                {
                    entry.Property(DateCreatedColumnName).CurrentValue = saveTime;
                }
            }
        }

        private void HandleNewISecuredByTenantEntries()
        {
            // Code related to ISecuredByTenant & row level security borrowed from:
            // https://blogs.msdn.microsoft.com/mvpawardprogram/2016/02/09/row-level-security-in-entityframework-6-ef6/

            var createdEntries = GetCreatedEntries();
            if (createdEntries.Any())
            {
                foreach (var createdEntry in createdEntries)
                {
                    var iSecuredByTenantEntry = createdEntry.Entity as ISecuredByTenant;
                    if (iSecuredByTenantEntry != null)
                    {
                        iSecuredByTenantEntry.UserId = _userId;
                    }
                }
            }
        }

        private IEnumerable<DbEntityEntry> GetCreatedEntries()
        {
            return ChangeTracker.Entries().Where(x => EntityState.Added.HasFlag(x.State));
        }

        public static WaterloggedDbContext Create()
        {
            return new WaterloggedDbContext();
        }

        public static class FilterNames
        {
            public const string SECURED_BY_TENANT = "SecuredByTenant";
        }
    }
}
